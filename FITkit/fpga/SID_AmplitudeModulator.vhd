-- -----------------------------------------------------------------------------
--! @file
--! @author Jan Stanek -- <xstane32@stud.fit.vutbr.cz>
--! @date 2012-06-05
--! @brief Amplitudovy modulator pro SID6581
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

--! @brief Knihovna zabudovanych komponent FPGA
--! @details Knihovna je pouzita kvuli vestavenym 18-ti bitovym nasobickam
library UNISIM;
use UNISIM.vcomponents.all;

entity AmplitudeModulator is
  port (
    WAVE : in std_logic_vector(11 downto 0); --! Vystup oscilatoru
    ENVELOPE : in std_logic_vector(16 downto 0); --! Vystup generatoru obalky
    ----
    VOICE : out std_logic_vector(11 downto 0) --! Vystupni modulovany signal
    );
end entity AmplitudeModulator;

architecture main of AmplitudeModulator is
  signal output: std_logic_vector(35 downto 0) := (others => '0');
  signal wave_inner: std_logic_vector(17 downto 0) := (others => '0');
  signal envel: std_logic_vector(17 downto 0) := (others => '0');
begin

  input_mod: block
  begin
    wave_inner <= ext(WAVE, 18);
    envel <= "0" & ENVELOPE;
  end block;

  multiplier: MULT18x18 port map (A => wave_inner, B => envel, P => output);

  --! @brief Uprava vysledku na vystup
  --! @details Zakladni vypocet je podle vzorce A * B / 2^16, pokud je B
  --! 16-tibitovy koeficient. Vysledek po nasobeni (A * B) je proto
  --! posunut o 16 bitu doprava, a protoze A je originalne 12-tibitove,
  --! je z vysledku odebrano 12 bitu.
  VOICE <= output(27 downto 16);

end architecture main;
