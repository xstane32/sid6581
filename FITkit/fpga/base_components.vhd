-- -----------------------------------------------------------------------------
--! @file base_components.vhd
--! @author Jan Stanek -- <xstane32@stud.fit.vutbr.cz>
--! @date 2012-06-04
--! @brief Zakladni a genericke stavebni prvky pro komponenty cipu SID6581
--! @details Soubor obsahuje deklarace a definice generickych komponent nejnizsi
--! urovne, pouzivane jako soucasti obvodu SID6581 a jeho soucastmi - napr.
--! generator povolovaciho signalu (zpomalovac hodin) a PWM.
-- -----------------------------------------------------------------------------

-- ####### PWM #######
library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

--! @brief Genericke rozhrani PWM.
entity PWM is
  generic ( BIT_WIDTH : integer := 8 );
  port (
    CLK : in std_logic; --! Systemove hodiny
    VAL : in std_logic_vector(BIT_WIDTH-1 downto 0); --! Modulated value
    PWM_OUT : out std_logic --! PWM output
    );
end entity PWM;

--! @brief Hlavni architektura PWM
architecture main of PWM is
  --! Akumulator
  signal acc : std_logic_vector (BIT_WIDTH-1 downto 0) := (others => '0');
begin

  accumulator: process(CLK)
  begin
    if (CLK'event) and (CLK = '1') then
      acc <= acc + 1;
    end if;
  end process;

  PWM_OUT <= '1' when (acc < VAL) else '0';

end architecture main;

-- ######## Generator povolovaciho signalu #######
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

--! @brief Genericky 'Generator povolovaciho signalu'
--! @details Komponenta pomoci akumulatoru generuje vzestupnou hranu jednou za
--! MAXVALUE tiku hodin a efektivne je tak zpomaluje. Umoznuje tudiz generovat
--! 'hodiny' ruzne frekvence.
entity ENGEN is
  generic ( MAXVALUE : integer );
  port (
    CLK : in std_logic; --! Systemove hodiny
    EN  : in std_logic; --! Povolovaci signal, umoznuje radit generatory do kaskady
    GEN : out std_logic --! Generovany signal
    );
end entity ENGEN;

--! @brief Architektura ENGEN
architecture main of ENGEN is
  signal acc : integer range 0 to MAXVALUE := 0;
begin

  accumulator: process(CLK)
  begin
    if (CLK'event) and (CLK = '1') then
      GEN <= '0';
      if (EN = '1') then
        if (acc = MAXVALUE) then
          acc <= 0;
          GEN <= '1';
        else
          acc <= acc + 1;
        end if;
      end if;
    end if;
  end process;

end architecture main;
