-- -----------------------------------------------------------------------------
--! @file
--! @author Jan Stanek -- <xstane32@stud.fit.vutbr.cz>
--! @date 2012-06-06
--! @brief Top level entita.
--! @details Zapojeni cipu SID6581 na FITkit - SPI, audio vystup apod.
-- -----------------------------------------------------------------------------

--! @brief Architektura top-level entity
architecture main of tlv_gp_ifc is

  --! @brief SPI dekoder
  component SPI_adc is
    generic (
      ADDR_WIDTH: integer := 8; --! Celkova sirka adresy
      DATA_WIDTH: integer := 8; --! Sirka dat
      --! Sirka adresy dospna pripojenemu zarizeni
      ADDR_OUT_WIDTH: integer := 4; 
      BASE_ADDR: integer := 16#10#; --! Bazova adresa zarizeni
      DELAY: integer := 0 --! Pocet taktu mezi READ_EN a platnou DATA_IN
      );
    port (
      -- Synchronizace
      CLK: in std_logic; --! Systemove hodiny
      
      -- Rozhrani SPI
      CS: in std_logic;
      DO: in std_logic;
      DO_VLD: in std_logic;
      DI: out std_logic;
      DI_REQ: in std_logic;

      -- Datove rozhrani
      --! @brief Adresa prijimajiciho zarizeni/registru
      ADDR : out std_logic_vector(ADDR_OUT_WIDTH-1 downto 0);
      --! @brief Data do FPGA
      DATA_OUT : out std_logic_vector(DATA_WIDTH-1 downto 0);
      --! @brief Data z FPGA
      DATA_IN : in std_logic_vector(DATA_WIDTH-1 downto 0);

      WRITE_EN : out std_logic; --! Platna DATA_OUT
      READ_EN : out std_logic --! Platna DATA_IN
      );
  end component SPI_adc;

  signal clk_1mhz: std_logic := '0';
  signal spi_addr_out: std_logic_vector(4 downto 0) := (others => '0');
  signal spi_data: std_logic_vector(7 downto 0) := (others => '0');
  signal spi_we: std_logic := '0';
  signal audio: std_logic_vector(11 downto 0) := (others => '0');
  signal pwm_out: std_logic := '0';

begin
  clkgen: entity work.ENGEN generic map (MAXVALUE => 39)
    port map (CLK => CLK, EN => '1', GEN => clk_1mhz);

  -- SPI dekoder. Porty na rozhrani jsou z balicku SPI
  spi: SPI_adc
  generic map (ADDR_WIDTH => 8, ADDR_OUT_WIDTH => 5, BASE_ADDR => 0)
  port map ( 
    CLK => CLK,

    CS => SPI_CS, DO => SPI_DO, DO_VLD => SPI_DO_VLD,
    DI => SPI_DI, DI_REQ => SPI_DI_REQ,

    ADDR => spi_addr_out, DATA_OUT => spi_data, DATA_IN => "00000000",
    WRITE_EN => spi_we, READ_EN => open
    );

  -- PWM
  pwm: entity work.PWM generic map (BIT_WIDTH => 12)
    port map (CLK => CLK, VAL => audio, PWM_OUT => pwm_out);

  -- SID6581
  sid: entity work.SID6581 port map (CLK_1MHz => clk_1mhz, SPI_CLK => CLK,
    ADDRESS => spi_addr_out, DATA => spi_data, WRITE_EN => spi_we,
    AUDIO_OUT => audio);

  -- Vystup
  X <= (7 => pwm_out, 9 => pwm_out, others => 'Z');

  -- Ostatni vystupni porty
  KIN  <= (others => 'Z');
  LE   <= 'Z';
  LRS  <= 'Z';
  LRW  <= 'Z';
  RA   <= (others => 'Z');
  RDQM <= 'Z';
  RCS  <= 'Z';
  RRAS <= 'Z';
  RCAS <= 'Z';
  RWE  <= 'Z';
  RCLK <= 'Z';
  RCKE <= 'Z';
  ADIN <= 'Z';

end architecture main;
