#Project setup
#=========================================================
set TESTBENCH_ENTITY "env_tb"
set ISIM_PRECISION "1ns"

#Run Simulation
#=========================================================
proc isim_script {} {
  divider add "Inputs"
  wave add /clk -color #00FF00 -radix bin -name "CLK_1MHz"
  wave add /gate -color #FF00FF -radix bin -name "Gate bit"
  
  divider add "Outputs"
  wave add /output -color #FF0000 -radix hex -name "Envelope"

  divider add "Inner"
  wave add /envelope/present_state -color #0000FF -radix ascii

  divider add "Accumulator"
  wave add /envelope/acc -color #FFFFFF -radix hex
  wave add /envelope/acc_en
  wave add /envelope/acc_up
  wave add /envelope/acc_rst
  wave add /envelope/acc_set
  wave add /envelope/acc_sus

  run 75 ms
}
