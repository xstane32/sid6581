library ieee;
use ieee.std_logic_1164.all;

entity env_tb is
end entity env_tb;

architecture beh of env_tb is
  signal clk: std_logic := '0';
  signal ad: std_logic_vector(7 downto 0) := "0010" & "0001";
  signal sr: std_logic_vector(7 downto 0) := "1000" & "0001";
  signal gate: std_logic := '0';

  signal output: std_logic_vector(16 downto 0) := (others => '0');

begin

  clk <= not clk after 500 ns; -- 1MHz

  envelope: entity work.EnvelopeGenerator
    port map (CLK_1MHz => clk, AD => ad, SR => sr,
              GATE => gate, ENV_MOD => output);

  gate <= '1' after 1 ms, '0' after 59 ms, '1' after 62 ms, '0' after 120 ms;

end architecture beh;
