-- -----------------------------------------------------------------------------
--! @file SID_Oscillator.vhd
--! @author Jan Stanek -- <xstane32@stud.fit.vutbr.cz>
--! @date 2012-06-04
--! @brief Oscillator a waveform selektor
--! @details Soucast cipu SID6581 - Oscilator a waveform selektor
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

--! @brief Balicek vlastnich funkci pro praci s vektory
use work.my_vector_pack.all;

entity Oscillator is
  port (
    CLK_1MHz : in std_logic; --! Kontrolni hodiny - 1MHz
    FREQ : in std_logic_vector(15 downto 0); --! Frekvence oscillatoru
    PW   : in std_logic_vector(11 downto 0); --! Delka pulsu obdelnikoveho signalu
    CONTROL : in std_logic_vector(7 downto 0); --! Vstup kontrolniho registru
    MSB_MOD : in std_logic; --! Bit pro synchronizaci a modulaci z jineho oscilatoru
  -- ----
    WAVE : out std_logic_vector(11 downto 0); --! Vystupni signal
    MSB_OUT : out std_logic --! Nejvyssi bit akumulatoru
    );
end entity Oscillator;

architecture main of Oscillator is
  --! @brief Akumulator
  signal acc : std_logic_vector(23 downto 0) := (others => '0');
  --! @brief Vystupy jednotlivych typu signalu
  signal output : std_logic_vector(11 downto 0) := (others => '0');
  --! @brief Linear Feedback Shift Register - generovani hluku
  signal lfsr : std_logic_vector(35 downto 0) := (others => '0');
  --! @brief Synchronizacni registr
  signal prev_msb_mod : std_logic := '0';
  --! @brief Signal synchronizace a resetu akumulatoru
  signal msb_sync : std_logic := '0';

  -- ### Aliasy ###
  -- Control register
  alias Sync : std_logic is CONTROL(1);
  alias RingMod : std_logic is CONTROL(2);
  alias Test : std_logic is CONTROL(3);
  alias TriangleWave : std_logic is CONTROL(4);
  alias SawtoothWave : std_logic is CONTROL(5);
  alias SquareWave   : std_logic is CONTROL(6);
  alias NoiseWave    : std_logic is CONTROL(7);
  -- Acc)mulator)
  alias MyMSB    : std_logic is acc(23);

begin
  --! @brief Hlidac synchronizace
  msbmod: process(CLK_1MHz)
  begin
    if (CLK_1MHz'event) and (CLK_1MHz = '1') then
	   msb_sync <= '0';
		if (MSB_MOD /= prev_msb_mod) and (MSB_MOD = '0') then
		  msb_sync <= '1';
		end if;
		prev_msb_mod <= MSB_MOD;
    end if;
  end process;

  --! @brief Akumulator, zaklad pro generovani signalu
  accumulator: process(CLK_1MHz)
  begin
    if (CLK_1MHz'event) and (CLK_1MHz = '1') then
      if (Test = '1') or (Sync = '1' and msb_sync = '1') then 
        acc <= (others => '0');
      else
        acc <= acc + ext(FREQ, 24);
      end if;
    end if;
  end process accumulator;

  wavegen: process(CLK_1MHz)
  begin
    if(CLK_1MHz'event) and (CLK_1MHz = '1') then
      output <= (11 => '1', others => '0');
      if (TriangleWave = '1') then
        if (RingMod = '1') then
          output <= svectorize(MSB_MOD, 12) xor acc(22 downto 11);
        else
          output <= svectorize(MyMSB, 12) xor acc(22 downto 11);
        end if;
      elsif (SawtoothWave = '1') then
        output <= acc(23 downto 12);
      elsif (SquareWave = '1') then
        if (acc(23 downto 12) < PW) then
          output <= (others => '1');
        else
          output <= (others => '0');
        end if;
      elsif (NoiseWave = '1') then
        output <= lfsr(35 downto 24);
      else
        null;
      end if;
    end if;
  end process;

  linearshift: process(CLK_1MHz)
  begin
    if (CLK_1MHz'event) and (CLK_1MHz = '1') then
      if (NoiseWave = '1') then
        if (acc(19) = '1') then
          lfsr <= lfsr(34 downto 0) & (lfsr(35) xnor lfsr(24));
        end if;
      end if;
    end if;
  end process;

  -- ### Vystup ###
  WAVE <= output;
  -- MSB pro synchronizaci
  MSB_OUT <= MyMSB;

end architecture main;
