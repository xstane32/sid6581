-- -----------------------------------------------------------------------------
--! @file
--! @author Jan Stanek -- <xstane32@stud.fit.vutbr.cz>
--! @date 2012-06-04
--! @brief Generator obalky pro SID6581
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.adsr_lut_pack.all;

entity EnvelopeGenerator is
  port (
    CLK_1MHz : in std_logic; --! Systemove hodiny
    AD : in std_logic_vector(7 downto 0); --! Attack - Decay
    SR : in std_logic_vector(7 downto 0); --! Sustain - Release
    GATE : in std_logic; --! Spoustec cyklu
  -- -----
    ENV_MOD : out std_logic_vector(16 downto 0) --! Vystupni modulace
     );
end entity EnvelopeGenerator;

architecture main of EnvelopeGenerator is
  --! Konecny automat kontrolujici obalku
  type fsm_state is (idle, attack, decay, sustain, release);
  attribute ENUM_ENCODING: string;
  attribute ENUM_ENCODING of fsm_state : type is "000 001 010 011 100";
  signal present_state, next_state : fsm_state := idle;

  --! Akumulator
  signal acc : std_logic_vector(32 downto 0) := (others => '0');
  --! @brief "Frekvence" akumulatoru
  --! @details Cislo, ktere se pricita k/odecita od akumulatoru. Cim vetsi,
  --! tim rychleji probiha aktualni faze generovani.
  signal acc_freq : std_logic_vector(23 downto 0) := (others => '0');
  --! @brief Akumulator enable
  signal acc_en : std_logic := '0';
  --! @brief Smer pocitani akumulatoru
  signal acc_up : std_logic := '1';
  --! @brief Reset akumulatoru
  signal acc_rst : std_logic := '0';
  --! @brief Set akumulatoru -- nastav na "maximalni" hodnotu
  signal acc_set : std_logic := '0';
  --! @brief Nastav sustain level
  signal acc_sus : std_logic := '0';

  --! @brief Sustain Level register
  --! @details Uchovava upravenou hodnotu Sustain
  signal sustain_level_reg : std_logic_vector(15 downto 0) := (others => '0');

  -- ### Aliasy ###
  alias AttackRate : std_logic_vector(3 downto 0) is AD(7 downto 4);
  alias DecayRate  : std_logic_vector(3 downto 0) is AD(3 downto 0);
  alias SustainRegister : std_logic_vector(3 downto 0) is SR(7 downto 4);
  alias ReleaseRate  : std_logic_vector(3 downto 0) is SR(3 downto 0);
  ----
  alias MyMSB : std_logic is acc(32);
  alias ModValue : std_logic_vector(16 downto 0) is acc(32 downto 16);

begin
  --! "Rozsireni" S)stainRegisteru na 16 bitu
  sustain_level_reg <= SustainRegister & SustainRegister & 
                       SustainRegister & SustainRegister;

 -- ####### FSM #######
 --! Synchronizacni logika FSM
  sync_logic: process(CLK_1MHz)
  begin
    if (CLK_1MHz'event) and (CLK_1MHz = '1') then
      present_state <= next_state;
    end if;
  end process sync_logic;

  --! Prechodova logika FSM
  next_state_logic: process(GATE, acc)
  begin
    next_state <= present_state;
    if (GATE = '1') then -- ADS
      case (present_state) is
        when release | idle =>
          next_state <= attack;
        when attack =>
          if (MyMSB = '1') then
            next_state <= decay;
          end if;
        when decay =>
          if (ModValue <= ("0" & sustain_level_reg)) then
            next_state <= sustain;
          end if;
        when others =>
          null;
      end case;
    else --Release, idle
      case (present_state) is
        when attack | decay | sustain =>
          next_state <= release;
        when release =>
          --Pokud je hodnota akumulatoru mensi nez nejvetsi mozny klesaci krok
          --(tzn. pri nejrychlejsim klesani v dalsim tiku podtece)
          if (acc < acc_freq) then
            next_state <= idle;
          end if;
        when others =>
          null;
      end case;
    end if;
  end process next_state_logic;

  --! Vystupni logika FSM
  output_logic: process(present_state, AD, SR, MyMSB, sustain_level_reg,
    ModValue)
  begin
    -- Defaultni hodnoty
    acc_freq <= (others => '0');
    acc_en <= '1';
    acc_up <= '1';
    acc_rst <= '0';
    acc_set <= '0';
    acc_sus <= '0';
    case (present_state) is
      when attack =>
        acc_freq <= ext(AttFreq(AttackRate), 24);
        if (MyMSB = '1') then
          acc_set <= '1';
        end if;
      when decay =>
        acc_freq <= ext(RelFreq(DecayRate), 24);
        acc_up <= '0';
        if (ModValue <= ("0" & sustain_level_reg)) then
          acc_sus <= '1';
        end if;
      when sustain =>
        acc_en <= '0';
      when release =>
        acc_freq <= ext(RelFreq(ReleaseRate), 24);
        acc_up <= '0';
      when idle =>
        acc_rst <= '1';
      when others =>
        null;
    end case;
  end process output_logic;

  -- ####### Akumulator #######
  process(CLK_1MHz)
  begin
    if (CLK_1MHz'event) and (CLK_1MHz = '1') then
      if (acc_en = '1') then
        if (acc_rst = '1') then
          acc <= (others => '0');
        elsif (acc_set = '1') then
          acc <= (32 => '1', others => '0');
        elsif (acc_sus = '1') then
          acc(32) <= '0';
          acc(31 downto 16) <= sustain_level_reg;
          acc(15 downto 0) <= sustain_level_reg;
        else
          if (acc_up = '1') then
            acc <= acc + ext(acc_freq, 33);
          else
            acc <= acc - ext(acc_freq, 33);
          end if;
        end if;
      end if;
    end if;
  end process;

  -- ####### Vystup #######
  ENV_MOD <= acc(32 downto 16);

end architecture main;
