-- -----------------------------------------------------------------------------
--! @file
--! @author Jan Stanek -- <xstane32@stud.fit.vutbr.cz>
--! @date 2012-06-05
--! @brief Prehravac SID6581
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity SID6581 is
  port (
    CLK_1MHz : in std_logic; --! System clock
    SPI_CLK : in std_logic; --! SPI clock
    ADDRESS : in std_logic_vector(4 downto 0); --! Adresa registru
    DATA : in std_logic_vector(7 downto 0); --! Data
    WRITE_EN : in std_logic; --! Indikator platnosti vstupnich dat
    ----
    AUDIO_OUT : out std_logic_vector(11 downto 0) --! Vystup - audio data
    );
end entity;

architecture main of SID6581 is
  type voice_long_registers is array (2 downto 0) of std_logic_vector(15 downto 0);
  type voice_short_registers is array (2 downto 0) of std_logic_vector(7 downto 0);
  type audio_registers is array (2 downto 0) of std_logic_vector(11 downto 0);
  type envelope_signals is array (2 downto 0) of std_logic_vector(16 downto 0);
  -- Kontrolni registry
  signal frequency: voice_long_registers := (others => (others => '0'));
  signal pulsewidth: voice_long_registers := (others => (others => '0'));
  signal control: voice_short_registers := (others => (others => '0'));
  signal adsr: voice_long_registers := (others => (others => '0'));
  -- Spojeni mezi oscilatory
  signal sync: std_logic_vector(2 downto 0) := (others => '0');
  -- Vystup oscilatoru
  signal wave: audio_registers := (others => (others => '0'));
  -- Vystup ADSR
  signal envel: envelope_signals := (others => (others => '0'));
  -- Vystup AmplitudeModulator
  signal voice: audio_registers := (others => (others => '0'));
  -- Vystupni mixer
  signal mix_inner: std_logic_vector(13 downto 0) := (others => '0');
  signal mix_out: std_logic_vector(11 downto 0) := (others => '0');

begin
  --! Hlasy
  voices: for i in 2 downto 0 generate

    osc: entity work.Oscillator port map (CLK_1MHz => CLK_1MHz, 
      FREQ => frequency(i), PW => pulsewidth(i)(11 downto 0), CONTROL => control(i),
      MSB_MOD => sync((i+2) mod 3), MSB_OUT => sync(i), WAVE => wave(i));

    adsr_gen: entity work.EnvelopeGenerator port map (CLK_1MHz => CLK_1MHz,
      AD => adsr(i)(15 downto 8), SR => adsr(i)(7 downto 0), 
      GATE => control(i)(0), ENV_MOD => envel(i));

    amp_mod: entity work.AmplitudeModulator 
      port map (WAVE => wave(i), ENVELOPE => envel(i), VOICE => voice(i));

  end generate;

  --! Adresovani registru
  adr: process(WRITE_EN)
  begin
    if (SPI_CLK'event) and (SPI_CLK = '1') then
      if (WRITE_EN = '1') then
        case (ADDRESS) is
          -- Voice 1
          when "00000" => frequency(0)(7 downto 0) <= DATA;
          when "00001" => frequency(0)(15 downto 8) <= DATA;
          when "00010" => pulsewidth(0)(7 downto 0) <= DATA;
          when "00011" => pulsewidth(0)(15 downto 8) <= DATA;
          when "00100" => control(0) <= DATA;
          when "00101" => adsr(0)(15 downto 8) <= DATA;
          when "00110" => adsr(0)(7 downto 0) <= DATA;
          -- Voice 2
          when "00111" => frequency(1)(7 downto 0) <= DATA;
          when "01000" => frequency(1)(15 downto 8) <= DATA;
          when "01001" => pulsewidth(1)(7 downto 0) <= DATA;
          when "01010" => pulsewidth(1)(15 downto 8) <= DATA;
          when "01011" => control(1) <= DATA;
          when "01100" => adsr(1)(15 downto 8) <= DATA;
          when "01101" => adsr(1)(7 downto 0) <= DATA;
          -- Voice 3
          when "01110" => frequency(2)(7 downto 0) <= DATA;
          when "01111" => frequency(2)(15 downto 8) <= DATA;
          when "10000" => pulsewidth(2)(7 downto 0) <= DATA;
          when "10001" => pulsewidth(2)(15 downto 8) <= DATA;
          when "10010" => control(2) <= DATA;
          when "10011" => adsr(2)(15 downto 8) <= DATA;
          when "10100" => adsr(2)(7 downto 0) <= DATA;

          when others => null;
        end case;
      end if;
    end if;
  end process;

  --! Mixer
  mix: process(CLK_1MHz)
  begin
    if (CLK_1MHz'event) and (CLK_1MHz = '1') then
      mix_inner <= voice(0) + voice(1) + voice(2);
      mix_out <= mix_inner(13 downto 2) + ("00" & mix_inner(13 downto 4));
    end if;
  end process;

  AUDIO_OUT <= mix_out;

end architecture main;
