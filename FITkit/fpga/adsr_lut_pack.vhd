-- -----------------------------------------------------------------------------
--! @file
--! @author Jan Stanek -- <xstane32@stud.fit.vutbr.cz>
--! @date 2012-06-04
--! @brief Balicek pomocnych funkci pro ADSR Envelope generator.
--! @details Balicek byl vytvoren pro zapouzdreni funkci generatoru kvuli
--! zprehledneni kodu.
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

--! @brief Balicek funkci pro ADSR Envelope Generator.
--! @details Funkce v tomto balicku zajistuji korektni preklad hodnoty v
--! prislusnem SID registru na hodnotu pricitanou k akumulatoru.
package adsr_lut_pack is
  function AttFreq(rate: std_logic_vector(3 downto 0)) return std_logic_vector;
  function RelFreq(rate: std_logic_vector(3 downto 0)) return std_logic_vector;
end adsr_lut_pack;

package body adsr_lut_pack is

  --! @brief Attack LUT
  --! @details Funkce prevadi hodnotu v registru na hodnotu pricitanou k
  --! akumulatoru. Je resena jako LUT, aby se nemusela syntetyzovat vypocetni
  --! logika.
  function AttFreq(rate : std_logic_vector(3 downto 0)) return std_logic_vector is
    variable int_rate : integer := 0;
    variable result : integer := 0;
  begin
    int_rate := conv_integer(rate);
    case (int_rate) is
      when 0 =>
        result := 16#20C49C#;
      when 1 =>
        result := 16#83127#;
      when 2 =>
        result := 16#41893#;
      when 3 =>
        result := 16#2BB0D#;
      when 4 =>
        result := 16#1B981#;
      when 5 =>
        result := 16#12B98#;
      when 6 =>
        result := 16#F6B9#;
      when 7 =>
        result := 16#D1B7#;
      when 8 =>
        result := 16#A7C6#;
      when 9 =>
        result := 16#431C#;
      when 10 =>
        result := 16#218E#;
      when 11 =>
        result := 16#14F9#;
      when 12 =>
        result := 16#10C7#;
      when 13 =>
        result := 16#598#;
      when 14 =>
        result := 16#35B#;
      when 15 =>
        result := 16#219#;
      when others =>
        null;
    end case;
    return conv_std_logic_vector(result, 24);
  end function;

  --! @brief Decay/Release LUT
  --! @details Viz AttFreq().
  function RelFreq(rate: std_logic_vector(3 downto 0)) return std_logic_vector is
    variable int_rate: integer := 0;
    variable result: integer := 0;
  begin
    int_rate := conv_integer(rate);
    case (int_rate) is
      when 0 =>
        result := 16#AEC34#;
      when 1 =>
        result := 16#2BB0D#;
      when 2 =>
        result := 16#15D86#;
      when 3 =>
        result := 16#E904#;
      when 4 =>
        result := 16#932B#;
      when 5 =>
        result := 16#63DD#;
      when 6 =>
        result := 16#523E#;
      when 7 =>
        result := 16#45E8#;
      when 8 =>
        result := 16#37ED#;
      when 9 =>
        result := 16#165F#;
      when 10 =>
        result := 16#B2F#;
      when 11 =>
        result := 16#6FE#;
      when 12 =>
        result := 16#598#;
      when 13 =>
        result := 16#1DD#;
      when 14 =>
        result := 16#11E#;
      when 15 =>
        result := 16#B3#;
      when others =>
        null;
    end case;
    return conv_std_logic_vector(result, 24);
  end function;

end adsr_lut_pack;
