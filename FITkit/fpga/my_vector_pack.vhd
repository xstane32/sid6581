-- -----------------------------------------------------------------------------
--! @file
--! @author Jan Stanek -- <xstane32@stud.fit.vutbr.cz>
--! @date 2012-06-14
--! @brief Balicek funkci pro praci s typy std_logic a std_logic_vector
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

--! @brief Balicek pro prevody funkci mezi typy std_logic a std_logic_vector
package my_vector_pack is
  function vectorize(scalar: IN std_logic; width: IN integer)
    return std_logic_vector;
  function svectorize(scalar: IN std_logic; width: IN integer)
    return std_logic_vector;
end my_vector_pack;

package body my_vector_pack is

  --! @brief Prevod std_logic na std_logic_vector s bezznamenkovym rozsirenim.
  --! @param[in] scalar Skalar k rozsireni.
  --! @param[in] width Sirka vysledneho vektoru.
  --! @return Vektor o sirce width a hodnote scalar.
  function vectorize(scalar: IN std_logic; width: IN integer) return std_logic_vector is
    variable temp: std_logic_vector(0 downto 0);
  begin
    temp(0) := scalar;
    return ext(temp, width);
  end function vectorize;

  --! @brief Prevod std_logic na std_logic_vector se znamenkovym rozsirenim.
  --! @param[in] scalar Skalar k rozsireni.
  --! @param[in] width Sirka vysledneho vektoru.
  --! @return Vektor o sirce width a hodnote scalar.
  function svectorize(scalar: IN std_logic; width: IN integer) return std_logic_vector is
    variable temp: std_logic_vector(0 downto 0);
  begin
    temp(0) := scalar;
    return sxt(temp, width);
  end function svectorize;

end my_vector_pack;
