#include <fitkitlib.h>
#include <string.h>

#define write_sample(addr, val) FPGA_SPI_RW_A8_D8(SPI_FPGA_ENABLE_WRITE, (addr), (val))

char flag_timer;

int ReadSdmpFrame(unsigned char *buffer, unsigned int *page, unsigned int *offset)
{
  unsigned int buffer_index = 1;
  
  /* Nacteni poctu polozek */
  FLASH_ReadData(buffer, 1, *page, *offset);
  (*offset)++;
  if ((*offset) >= PAGE_SIZE)
  {
    (*offset) -= PAGE_SIZE;
    (*page)++;
  }

  if (buffer[0] == 0xFF) //Konec souboru
  {
    return 0;
  }

  int i;
  for (i = 0; i < buffer[0]; i++)
  {
    FLASH_ReadData((buffer + buffer_index), 2, *page, *offset);
    (*offset) += 2;
    if ((*offset) >= PAGE_SIZE)
    {
      (*offset) -= PAGE_SIZE;
      (*page)++;
    }
    buffer_index += 2;
  }
  /* nacten cely radek */
  return 1;
}

void PlaySdmpFromFlash(void)
{
  unsigned char buf[43]; /* maximum polozek = 42, +1 na pocet polozek na radku */
  unsigned int i, page, offset, tmr;
  unsigned int buf_index;
  
  term_send_str("Playing... ");

  page = FLASH_USER_DATA_PAGE;
  offset = 0;

  tmr = 0;

  while (ReadSdmpFrame((unsigned char *) &buf, &page, &offset))
  {
    /* odesilej do FPGA */
    buf_index = 1;
    for (i = 0; i < buf[0]; i++, buf_index += 2)
    {
      write_sample(buf[buf_index], buf[buf_index+1]);
    }

    delay_ms(20);

    tmr++;
    if ((tmr%50) == 0)
    {
      term_send_num(tmr/50);
      term_send_char(' ');
      terminal_idle();
    }
  }
  /* po poslednim prehrani vynulovat SID registry */
  for (i = 0; i <= 20; i++)
  {
    write_sample(i, 0);
  }

  //Dohrali jsme
  term_send_str_crlf("Finish");
}

void print_user_help(void)
{
  unsigned int pages = FLASH_availablepages();
  term_send_str(" FLASH SDMP ... nahrani SID dumpu (*.sdmp), max. velikost ");
  term_send_num((unsigned long)(pages) * (unsigned long)(PAGE_SIZE));
  term_send_str_crlf("B.");
}

unsigned char decode_user_cmd(char *cmd_ucase, char *cmd)
{
  if (strcmp(cmd_ucase, "FLASH SDMP") == 0)
  {
    FLASH_WriteFile(FLASH_USER_DATA_PAGE, "SIDdump", "*.sdmp", FLASH_availablepages());
    return (USER_COMMAND);
  }
  else if (strcmp(cmd_ucase, "PLAY") == 0)
  {
    PlaySdmpFromFlash();
  }
  return (CMD_UNKNOWN);
}

void fpga_initialized() {}

int main(void)
{
  initialize_hardware();

  flag_timer = 0;
  CCTL0 = CCIE;  // povol preruseni pro casovac (rezim vystupni komparace) 
  CCR0 = 0x0004; // nastav po kolika ticich (32768 / 4 = 8192 Hz) ma dojit k preruseni
  TACTL = TASSEL_1 + MC_2; // ACLK (f_tiku = 32768 Hz), nepretrzity rezim

  set_led_d5(1);

  while(42)
  {
    delay_ms(1);
    terminal_idle(); //obsluha terminalu
  }

  return 0;
}

/* obsluha preruseni */
interrupt (TIMERA0_VECTOR) Timer_A (void)
{
  CCR0 += 0x0004;
  flag_timer |= 1;
}
