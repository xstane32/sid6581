# Commodore SID6581
## Reimplementace ve VHDL

Toto je reimplementace čipu SID6581 ve VHDL, zpracováváno jako projekt do předmětu Seminář VHDL na VUT v Brně.
Pokud se vás tento předmět také týká, inspirujte se, ale snažte se neopisovat,
jednak se toho moc nenaučíte a druhak z toho budete mít pravděpodobně problémy
se zápočtem.

## Adresářová struktura
### FITkit/

V adresáři FITkit se nachází samotná implementace přehrávače, ve formátu projektu pro FITkit.

### Extras/

V adresáři Extras se nacházi skript na převod výstupu programu [SIDDump](http://covertbitops.c64.org/ "Covert Bitopts") na binarní formát zpracovávaný implementovaným přehrávačem.

## License
Copyright (c) 2012, Jan Stanek (xstane32@stud.fit.vutbr.cz, khardix@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the organization nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
